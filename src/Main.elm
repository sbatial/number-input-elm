module Main exposing (main)

import Browser
import Char exposing (isDigit)
import Html exposing (Html, br, div, input, text)
import Html.Attributes exposing (style, type_, value)
import Html.Events exposing (onInput)
import List exposing (intersperse)
import String exposing (replace)


type Msg
    = Count String


type Input
    = Input (Maybe Float) String


init : Model
init =
    { input = Input Nothing "" }


main : Program () Model Msg
main =
    Browser.sandbox { init = init, update = update, view = view }


type alias Model =
    { input : Input }


update : Msg -> Model -> Model
update msg _ =
    case msg of
        Count i ->
            { input = Input (numberValidation i) (tryExtractNumber i) }


{-| Tries to get as much of a parseable number string as possible out of the given string by filtering out everything that cannot be reasonably assumed to be a number
-}
tryExtractNumber : String -> String
tryExtractNumber =
    String.filter (\c -> isDigit c || '.' == c || ',' == c || '_' == c)


{-| Try to convert the given string into a number as graciously as possible.
This includes numbers with (one) comma, one decimal point and underscores.
-}
numberValidation : String -> Maybe Float
numberValidation =
    String.toFloat
        << String.filter (\c -> c /= '_')
        << replace "," "."


view : Model -> Html Msg
view m =
    let
        (Input res count) =
            m.input
    in
    div []
        (intersperse (br [] [])
            [ numberInput count res
            , text ("Count (Maybe Float): " ++ (String.fromFloat << Maybe.withDefault 0) res)
            , text ("Count (String): " ++ count)
            ]
        )


numberInput : String -> Maybe a -> Html Msg
numberInput val valid =
    input
        ([ onInput Count, type_ "text", value val ]
            ++ (if valid /= Nothing || String.length val == 0 then
                    []

                else
                    [ style "border" "solid red 1px" ]
               )
        )
        []
